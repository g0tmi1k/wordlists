## Wordlists

```console
./
├── Credentials/
│   ├──Passwords.txt
│   ├──Usernames.txt
├── Discovery
│   ├──Web-Local/
│   │   ├── Apache.txt
│   │   ├── IIS.txt
│   │   ├── Windows.txt <--- IIS?
│   │   ├── Linux.txt   <--- Apache?
│   ├──Web-Remote/
│   │   ├── Apache.txt
│   │   ├── IIS.txt
│   │   ├── Windows.txt <--- IIS?
│   │   ├── Linux.txt   <--- Apache?
├── Vulnerabilities
│   ├──Code-Analysis
│   │   ├── PHP.txt
│   ├──Web
│   │   ├── LFI.txt
│   │   ├── RFI.txt
│   │   ├── SQLi.txt
```
